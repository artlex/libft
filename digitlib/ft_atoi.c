/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 20:07:55 by bcherkas          #+#    #+#             */
/*   Updated: 2018/12/01 17:17:43 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int				min;
	unsigned long	sum;

	sum = 0;
	min = 1;
	while (ft_isspace(*str))
		str++;
	if (*str == '-')
		min *= -1;
	if (*str == '-' || *str == '+')
		str++;
	while (*str && ft_isdigit(*str))
	{
		sum = sum * 10 + (*str - '0');
		str++;
	}
	return ((int)sum * min);
}
