---Libft--

Library created my myself to use it in Unit Factory projects.
This library has as copies of libc functions as new usefull functions non-present in standart library.

For example: 

	ft_strsplit: splits string by divider into two dimensioned array 
	
	ft_strtrim: cuts spaces on start and end of string
	
	ft_strsub: returns substring of chosen number of elements
	
	get_next_line: allocates string and writes there a line read from file descriptor
	
There is full list of functions in this library:

Without classification:

    ft_printf (without float numbers and $ flag)
    get_next_line

 Memlib:
 
	ft_bzero
	ft_delarr
	ft_memalloc
	ft_memccpy
	ft_memchr
	ft_memcmp
	ft_memcpy
	ft_memdel
	ft_memmove
	ft_memset
 Charlib:
 
	ft_isalnum
	ft_isalpha
	ft_isaii
	ft_isdigit
	ft_isprint
	ft_isspe
	ft_tolower
	ft_toupper
 Digitlib:
 
	ft_abs
	ft_atof
	ft_atoi
	ft_ftoa
	ft_intlen
	ft_itoa
 Strlib:
 
	ft_stat
	ft_sthr
	ft_stlr
	ft_stmp
	ft_stpy
	ft_strdel
	ft_strdup
	ft_strequ
	ft_striter
	ft_striteri
	ft_strjoin
	ft_strat
	ft_strlen
	ft_strmap
	ft_strmapi
	ft_strat
	ft_strmp
	ft_strpy
	ft_strnequ
	ft_strnew
	ft_strnstr
	ft_strhr
	ft_strrev
	ft_strsplit
	ft_strsplit_lst
	ft_strstr
	ft_strsub
	ft_strtrim
 Textlib:
 
	ft_puhar
	ft_puhar_fd
	ft_putendl
	ft_putendl_fd
	ft_putnbr
	ft_putnbr_fd
	ft_putstr
	ft_putstr_fd
 Lstlib:
 
	ft_lstadd
	ft_lstdel
	ft_lstdeontent
	ft_lstdelone
	ft_lstiter
	ft_lstlen
	ft_lstmap
	ft_lstnew
	ft_lstpushbk
	ft_lstsort
